public class Circulo  implements Draggable {
     float x;
     float y;
     float radio;
     boolean drag;
     float dragX;
     float dragY;
     
     Circulo(float a, float b){
         this.x = a;
         this.y = b;
         this.radio = 30;
         drag = false;
         dragX = 0;
         dragY = 0;
     }
     
     boolean isMouseSobreFigura(){
         return adentro(mouseX, mouseY);
     }
     
     boolean adentro(float ix, float iy){
         return ( dist(this.x, this.y, ix, iy) < this.radio );
     }
     
     void draw(){
        ellipseMode(CENTER);
        ellipse(x,y,2*radio,2*radio);
     }
     
     void mousePressed(){
         drag = adentro(mouseX, mouseY);
         if(drag){
             dragX = mouseX - x;
             dragY = mouseY - y;
         }
     }
     void mouseDragged(){
        if(drag){
           x = mouseX -dragX;
           y = mouseY - dragY;
        }
     }
     
     void mouseReleased(){
        drag = false;
     }
     
}