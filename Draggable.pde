interface Draggable{
     boolean isMouseSobreFigura();
     boolean adentro(float ix, float iy);
     void draw();
     void mousePressed();
     void mouseReleased();
     void mouseDragged();

}