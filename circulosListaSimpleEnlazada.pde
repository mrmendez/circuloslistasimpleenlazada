Draggable draggable;
PFont p;
NullDraggableObject nullDraggableObject;
ArrayList draggables;
PImage fondo;

void setup(){
    size(420,500);
    smooth();
    nullDraggableObject = new NullDraggableObject();
    draggables = new ArrayList();
    for (int i=0; i < 10   ;i++){
      draggables.add(new Circulo(random(width), random(height/2)));
    }
    fondo = loadImage("universo.jpeg");
    p = createFont("Arial",18);
    textFont(p);
}

void draw(){
   image(fondo,0,0);
   stroke(255);
   fill(0,255,0);
   text("OBJETOS EN LISTA SIMPLEMENTE ENLAZADA",10,height-40);
   noFill();
   strokeWeight(3);
   draggable = nullDraggableObject;
   for(int i=0; i < draggables.size() ;i++){
       Draggable d = (Draggable)draggables.get(i);
       d.draw();
       if( d.isMouseSobreFigura() )
          draggable = d;
   }
}

void mousePressed(){
   draggable.mousePressed();
}

void mouseDragged(){
   draggable.mouseDragged();
}

void mouseReleased(){
   draggable.mouseReleased();
}
